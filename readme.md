# GitPower

**Made by Th�o & Nico**



[![forthebadge](https://forthebadge.com/images/badges/built-by-developers.svg)](https://forthebadge.com)

[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)

----------------
Ce projet consiste en un site web permettant de stocker et de partager des fichiers contenant du code.

Les utilisateurs pourront avoir un compte leur permettant de se connecter.
Ils pourront consulter le code, les modifier et les commenter.

----------------

## apercu de la page d'accueil du site

![image 1 site web](Public/assets/img/images%20readme/siteweb1.png)

## apercu de la liste des fichiers

![image 2 site web](Public/assets/img/images%20readme/siteweb2.png)


## Etat du projet
**Projet Termin�**  :heavy_check_mark:

##### Am�liorations futures possibles :
* choix de l'avatar pour les utilisateurs
* recherches par utilisateurs et par projets
* t�l�chargement des fichiers de code sans faire de copier/coller
* impl�menter une r�elle arborescence pour chaque projet

## License

&copy; Open Source :+1:

## Installation

* **Sous Unix** :penguin:

```
php composer-setup.php --install-dir=bin --filename=composer
sudo mv composer.phar /usr/local/bin/composer
```
* **Sous Windows** :poop:

[T�l�chargez la derni�re version de Composer ](https://getcomposer.org/Composer-Setup.exe "T�l�chargez la derni�re version de Composer ")

## D�marrage

* **Sous Unix / Windows** :penguin:/:poop:

```
cd Chemin_du_fichier/Public
php -S localhost:8080
```

## D�velopp� avec :

* PHP
* HTML
* CSS
* JAVASCRIPT
* SQLite

#### Librairies utilis�es :

* [Bootstrap](https://getbootstrap.com/ "Bootstrap")
* [RainbowJS](https://craig.is/making/rainbows "RainbowJS")
* [PHPUnit](https://phpunit.de/ "PHPUnit")


