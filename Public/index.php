<?php
/**
 * Created by PhpStorm.
 * Contributor: Theo
 * Date: 03/05/2019
 * Time: 00:09
 */

define('ACCESSIBLE', '1');

function var_d($data) {
    echo '<pre>' , var_dump($data) , '</pre>';
}

use Josantonius\Session\Session;
use App\Utils;

require "../vendor/autoload.php";
ini_set('display_errors',1);
define('ROOT', str_replace('\\', '/', dirname(__DIR__)));

$loader = new Twig_Loader_Filesystem(ROOT. '/Views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Session::init();

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addGlobal('INCLUDE_RAINBOW_DIR', "/assets/rainbow");
    $twig->addGlobal('INCLUDE_ICON_DIR', "/assets/img/icon/");
    $twig->addGlobal('INCLUDE_JS_DIR', "assets/js");
    $twig->addGlobal('USER_ID', \App\Controller\Controller::getUserId() );
    $twig->addFilter(new Twig_Filter('getExtension', function($fullName){
        return \App\Utils::getExtenstion($fullName);
    }));
    $twig->addFilter(new Twig_Filter('getNameWithoutExtension', function($fullName){
        return \App\Utils::getNameWithoutExtension($fullName);
    }));
    $twig->addFilter(new Twig_Filter('getTimeDiffAsString', function($time1, $time2){
        return \App\Utils::getTimeDiffStr($time1, $time2);
    }));
    $twig->addFilter(new Twig_Filter('canComment', function($project){
        return Project::userIsAtLeastDeveloper($project, \App\Controller\Controller::getUserId());
    }));
    $twig->addFilter(new Twig_Filter('getRole', function($role){
        return Utils::getRole($role);
    }));
});

Flight::before('start', function(&$params, &$output){
    ORM::configure('sqlite:base.sqlite3');
    ORM::rawExecute("PRAGMA foreign_keys = ON;");
});

Flight::start();
