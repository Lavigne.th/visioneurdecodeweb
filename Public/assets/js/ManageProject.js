$(function(){
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
    })

    var utils = {
        echo: function(data) {
            document.querySelector('#php').innerHTML = data;
        },
        getXHR: function() {
            try {
                return new XMLHttpRequest();
            }
            catch (e) {
                try {
                    return new ActiveXObject("Msxml2.XMLHTTP");
                }
                catch (e) {
                    try {
                        return new ActiveXObject("Microsoft.XMLHTTP");
                    } catch (e) {
                        alert("Your browser broke!");
                        return false;
                    }
                }
            }
        },
        listener: function(array, type, action) {
            for(var yu = 0; yu < array.length; yu++) {
                array[yu].addEventListener(type, action);
            }
        },
        noListener: function(array, type, action) {
            for(var yu = 0; yu < array.length; yu++) {
                array[yu].removeEventListener(type, action);
            }
        },
        echoMessage: function(message, type) {
            var popup =
                '<div class="alert alert-' + type + ' fade show m-3" role="alert"> ' +
                '<strong>' + message + '</strong>' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
                '</div>';
            this.echo(popup);
        },
        get: function(field) {
            var url = new URL(window.location.href);
            return url.searchParams.get(field);
        },
        clear: function (inputs) {
            for(var pop = 0; pop < inputs.length; pop++) {
                inputs[pop].value = "";
            }
        }
    }

    createContributor = function(name, surname, pseudo, tel, id, address, city, email, avatar) {
        idProject = document.querySelector("#search-result").getAttribute('data-project-id');
        document.querySelector('#search-result').innerHTML +=
            "<li class=\"list-group-item\">" +
            "    <div class=\"row w-100\">" +
            "        <div class=\"col-12 col-sm-6 col-md-3 px-0\">" +
            "            <img src=\"" + avatar + "\" alt=\"" + name + "\" width=\"100px\" height=\"100px\" class=\"rounded-circle mx-auto d-block img-fluid\">" +
            "            <div class=\"text-muted text-center small\">" + pseudo + "</div>" +
            "        </div>" +
            "        <div class=\"col-12 col-sm-6 col-md-9 text-center text-sm-left\">" +
            "            <div>" +
            "                <a href=\"/project/contributor/add/" + id + "/" + idProject + "\">" +
            "                    <span class=\"fas fa-user-plus fa-2x text-success float-right pulse\" title=\"online now\"></span>" +
            "                </a>" +
            "                <label class=\"name lead\">" + surname + " " + name + "</label>" +
            "            </div>" +
            "            <div>" +
            "                <span class=\"fa fa-map-marker fa-fw text-muted\" data-toggle=\"tooltip\" title=\"\" data-original-title=\"" + address + " " + city + "\"></span>" +
            "                <span class=\"text-muted\">" + address + " " + city + "</span>" +
            "            </div>" +
            "            <div>" +
            "                <span class=\"fa fa-phone fa-fw text-muted\" data-toggle=\"tooltip\" title=\"\" data-original-title=\"" + tel + "\"></span>" +
            "                <span class=\"text-muted small\">" + tel + "</span>" +
            "            </div>" +
            "            <div>" +
            "                <span class=\"fa fa-envelope fa-fw text-muted\" data-toggle=\"tooltip\" data-original-title=\"\" title=\"\"></span>" +
            "                <span class=\"text-muted small text-truncate\">" + email + "</span>" +
            "            </div>" +
            "        </div>" +
            "    </div>" +
            "</li>";
    }



    createSearchResult = function(results) {
        document.querySelector('#search-result').innerHTML = "";
        for(i = 0; i < results.length; i++) {
            createContributor(results[i]['name'], results[i]['surname'], results[i]['pseudo'], results[i]['tel'], results[i]['id'], results[i]['address'], results[i]['city'], results[i]['email'], results[i]['avatar']);
        }
    }



    search =  function (e) {
        e.preventDefault();
        var search = document.querySelector('#search-pseudo').value;
        if(search.length < 1) {
            utils.echoMessage("Au moin 3 characteres", "warning");
            return false;
        }
        var xhr = utils.getXHR();

        xhr.onreadystatechange = function () {
            if(xhr.readyState === 4) {
                if(xhr.status !== 200) {
                    alert("erreur");
                } else {

                    //utils.echo(xhr.responseText);
                    var resp = JSON.parse(xhr.responseText);
                    if(resp === "false") {
                        utils.echoMessage(p)
                    } else {
                        createSearchResult(resp['result']);
                        console.log(resp['resp']);
                    }
                }
            }
        }

        xhr.open("POST", "/project/contributor/search", true);

        var data = new FormData();
        data.append("pseudo", search);

        xhr.send(data);
    };



    //fileInput = document.querySelector('#file');
    fileInput2 = document.querySelector('#updated-file');
    fields = document.querySelectorAll('*[id^="fileNameContainer_"]');
    actions = document.querySelectorAll('.action');

    fileChanged  = function(e) {
        console.log(e.srcElement.getAttribute('id'))
        path = e.srcElement.value.split('\\');
        document.querySelector('label[for="'+ e.srcElement.getAttribute('id') + '"]').textContent = path[path.length - 1];
    };

    nameClicked = function(e) {
        e.srcElement.style.display = 'none';
        e.srcElement.closest('td').querySelector('form').style.display = 'initial';
    }

    cancelClicked = function(e) {
        e.srcElement.closest('form').style.display = 'none';
        e.srcElement.closest('td').querySelector('span.fileName').style.display = 'initial';
    }

    fields.forEach(function(element) {
        fileName = element.querySelector('span.fileName');
        fileName.addEventListener('click', nameClicked);
        fileFormCancelBtn = element.querySelector('button.cancel').addEventListener('click', cancelClicked);
        element.querySelector('form').style.display = 'none';
    });

    actions.forEach(function(element) {
        element.querySelector('.upload-btn').addEventListener('click', function (e) {
            $('#modal').modal('toggle');
            document.querySelector('#modal').querySelector('.modal-title').innerHTML = e.srcElement.getAttribute('data-fileName');
            document.querySelector('#modal').querySelector('form').setAttribute('action', "/file/update/" + e.srcElement.getAttribute('data-fileID'))
        })
    })

    document.querySelector('#modal .submit-btn').addEventListener('click', function (e) {
        console.log()

        valid = true;
        if(document.querySelector('#modal [name="title"]').value.length <= 2) {
            valid = false;
            document.querySelector('#modal [name="title"]').classList.add('is-invalid');
            document.querySelector('#modal [name="title"]').classList.remove('is-valid');
            document.querySelector('#title-feedback').style.display = "initial";
        } else {
            document.querySelector('#modal [name="title"]').classList.add('is-valid');
            document.querySelector('#modal [name="title"]').classList.remove('is-invalid');
            document.querySelector('#title-feedback').style.display = "none";
        }
        if(document.querySelector('#modal [name="comment-content"]').value.length <= 2) {
            valid = false;
            document.querySelector('#modal [name="comment-content"]').classList.add('is-invalid');
            document.querySelector('#modal [name="comment-content"]').classList.remove('is-valid');
            document.querySelector('#comment-feedback').style.display = "initial";
        } else {
            document.querySelector('#modal [name="comment-content"]').classList.add('is-valid');
            document.querySelector('#modal [name="comment-content"]').classList.remove('is-invalid');
            document.querySelector('#comment-feedback').style.display = "none";
        }
        if(document.querySelector('#modal [name="file"]').value.length <= 2) {
            valid = false;
            document.querySelector('#modal [name="file"]').classList.add('is-invalid');
            document.querySelector('#modal [name="file"]').classList.remove('is-valid');
            document.querySelector('#updated-file-feedback').style.display = "initial";
        } else {
            document.querySelector('#modal [name="file"]').classList.add('is-valid');
            document.querySelector('#modal [name="file"]').classList.remove('is-invalid');
            document.querySelector('#updated-file-feedback').style.display = "none";
        }
        if(valid)
            document.querySelector('#modal form').submit();
    })

    //fileInput.addEventListener('change', fileChanged);
    fileInput2.addEventListener('change', fileChanged);
    document.querySelector('#search-form').addEventListener('submit',search );
});
