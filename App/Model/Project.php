<?php
/**
 * Created by PhpStorm.
 * Contributor: Theo
 * Date: 19/05/2019
 * Time: 19:46
 */

defined('ACCESSIBLE') or die('No direct script access.');

class Project extends \Model {

    public static $_table = 'Project';

    public function Project_contributor() {
        return $this->has_many('Project_contributor', 'project_id');
    }

    public function files() {
        return $this->has_many('File', 'project_id');
    }

    /*
     * Détermine si l'utilisateur passé en parametre est l'administrateur du projet
     */
    public  static function isAdminOfProject($idProject, $idContributor) {
        $projectContributor = Model::factory('ProjectContributor')
            ->where('project_id', $idProject)
            ->where('contributor_id', $idContributor)
            ->where('role', 'admin')
            ->findOne();
        if($projectContributor)
            return true;
        else
            return false;
    }

    /*
     * Détermine si l'utilisateur à accès au projet
     */
    public static function userCanViewProject($project, $userId) {
        if($project->visibility == 'public') {
            return true;
        } else if($project->visibility == 'interne' && $userId != null) {
            return true;
        } else if($project->visibility == 'private' && $userId != null) {
            $projectContributor = Model::factory('ProjectContributor')
                ->where('project_id', $project->id)
                ->where('contributor_id', $userId)
                ->findOne();
            if($projectContributor)
                return true;
        }
        return false;
    }

    /*
     * Détermine si le contributeur passée ne parametre est au moin dévellopeur du projet
     */
    public static function userIsAtLeastDeveloper($project, $userId) {
        $projectContributor = Model::factory('ProjectContributor')
            ->where('project_id', $project->id)
            ->where('contributor_id', $userId)
            ->findOne();

        if($projectContributor && $projectContributor->role == ProjectContributor::$ADMIN_ROLE || $projectContributor && $projectContributor->role == ProjectContributor::$DEV_ROLE)
            return true;
        else
            return false;
    }

    /*
     * Retourne l'administrateur du projet passer en parametre
     */
    public static function getAdminOfProject($idProject) {
        $projectContributor = Model::factory('ProjectContributor')
            ->where('project_id', $idProject)
            ->where('role', 'admin')
            ->findOne();

        if($projectContributor) {
            return Model::factory('Contributor')->findOne($projectContributor->contributor_id);
        }
        return null;
    }

    /*
     * Retourne tout les contributeurs d'un projet.
     */
    public static function getAllContributor($idProject) {
        $query = "SELECT Contributor.id , Contributor.pseudo, Contributor.avatar, Contributor.email, Contributor.name, Contributor.surname, Contributor.tel, Contributor.address, Contributor.city, Project_contributor.role
        FROM Contributor, Project_contributor 
        WHERE Contributor.id = Project_contributor.contributor_id 
        AND Project_contributor.project_id = ?;";
        $res = null;
        if(ORM::rawExecute($query, [$idProject])) {
            $statement = ORM::get_last_statement();
            $res = $statement->fetchAll(\PDO::FETCH_ASSOC);
        }
        return $res;
    }


}