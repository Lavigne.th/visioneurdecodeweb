<?php
/**
 * Created by PhpStorm.
 * User: Theo
 * Date: 25/05/2019
 * Time: 16:38
 */

defined('ACCESSIBLE') or die('No direct script access.');

class File extends \Model {


    public static $_table = 'File';

    public function Supported_format() {
        return $this->belongs_to('Supported_format', 'format_id');
    }

    public function project() {
        return $this->belongs_to('project', 'project_id');
    }
}