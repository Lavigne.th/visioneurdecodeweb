<?php
/**
 * Created by PhpStorm.
 * Contributor: Theo
 * Date: 19/05/2019
 * Time: 19:46
 */

defined('ACCESSIBLE') or die('No direct script access.');

class CommentAssos extends \Model {

    public static $_table = 'Comment_assos';

    public function Comment() {
        return $this->belongs_to('Comment', 'id');
    }

    /*
     * Renvoie les l'objet associé à l'association.
     */
    public static function getAssociatedItem($object, $idCommentent) {
        $assos = Model::factory('CommentAssos')->findOne($idCommentent);
        if($assos)
            return Model::factory($object)->findOne($assos->item_id);
        return null;
    }

}