<?php

defined('ACCESSIBLE') or die('No direct script access.');

class ProjectContributor extends \Model {

    public static $_table = 'Project_contributor';
    public static $ADMIN_ROLE = 'admin', $DEV_ROLE = 'dev', $VISITOR_ROLE = 'visitor';

    public function Project() {
        return $this->belongs_to('Project', 'project_id');
    }
    public function Contributor() {
        return $this-> belongs_to('Contributor');
    }
}