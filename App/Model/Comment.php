<?php
/**
 * Created by PhpStorm.
 * Contributor: Theo
 * Date: 19/05/2019
 * Time: 19:46
 */

defined('ACCESSIBLE') or die('No direct script access.');

class Comment extends \Model {

    public static $_table = 'Comment';

    public function Comment_assos() {
        return $this->has_many('Comment_assos', 'comment_id');
    }

    /*
     * Renvoie les contributeurs associés au commentaires.
     */
    public static function getAssociatedContributors($comments) {

        $contributorIds = [];
        $contributors = [];
        foreach ($comments as $c) {
            if(!in_array($c['contributor_id'], $contributorIds))
                $contributorIds[] = $c['contributor_id'];
        }
        foreach ($contributorIds as $id) {
            $contributors[$id] = Model::factory('Contributor')->findOne($id);
        }
        return $contributors;
    }
}