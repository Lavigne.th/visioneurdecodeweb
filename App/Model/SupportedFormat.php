<?php
/**
 * Created by PhpStorm.
 * User: Theo
 * Date: 25/05/2019
 * Time: 16:37
 */

defined('ACCESSIBLE') or die('No direct script access.');

use App\Utils;

class SupportedFormat extends \Model {


    public static $_table = 'Supported_format';

    public function File()
    {
        return $this->has_many('File', 'format_id');
    }

    /*
     *  Creer en DB tout les format supportés (Supported_format).
     */
    public static function createAllSupportedTypes() {
        $files = scandir(ROOT.'/Public/assets/rainbow/language/');
        for ($i = 0; $i < sizeof($files); $i++) {
            if($files[$i][0] == '.' ||$files[$i] == 'generic.js' ) {
                unset($files[$i]);
            } else {
                $files[$i] = explode('.', $files[$i])[0];
            }
        }

        $extensions = [
            'coffeescript' => 'coffee',
            'csharp' => 'cs',
            'haskell' => 'hs',
            'javascript' =>'js',
            'python' => 'py',
            'ruby' => 'rb',
            'scheme' => 'scm',
            'shell' => 'sh',
            'smalltalk' => 'st'
        ];
        try {
            ORM::get_db()->beginTransaction();

            foreach($files as $language) {
                $format = Model::factory('SupportedFormat')->create();
                $format->name = $language;
                if(isset($extensions[$language]))
                    $extension = $extensions[$language];
                else
                    $extension = $language;
                $format->extension = $extension;
                $format->icon = '/assets/img/supported_format/'.$language.'.png';
                $format->save();
            }

            ORM::get_db()->commit();
        } catch (Exception $e) {
            ORM::get_db()->rollBack();
            echo "Failed: " . $e->getMessage();
            exit();
        }
    }
}