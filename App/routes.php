<?php
/**
 * Created by PhpStorm.
 * Contributor: Theo
 * Date: 03/05/2019
 * Time: 00:10
 */

defined('ACCESSIBLE') or die('No direct script access.');

/************************************ INDEX **************************************************/

Flight::route('/', array('IndexController', 'index'));

Flight::route('POST /login', array('IndexController', 'login'));

Flight::route('GET /disconnect', array('IndexController', 'disconnect'));

Flight::route('GET /register', array('IndexController', 'register_form'));

Flight::route('POST /register', array('IndexController', 'register'));

/************************************ PROJECT **************************************************/

Flight::route('GET /project/add', array('ProjectController', 'add_form'));

Flight::route('POST /project/add', array('ProjectController', 'add'));

Flight::route('GET /project/list/@idContributor', array('ProjectController', 'proj_list'));

Flight::route('GET /project/delete/@idProject', array('ProjectController', 'delete'));

Flight::route('GET /project/manage/@idProject', array('ProjectController', 'manage'));

Flight::route('GET /project/edit/@idProject', array('ProjectController', 'edit_form'));

Flight::route('POST /project/edit/@idProject', array('ProjectController', 'edit'));

Flight::route('POST /project/add/comment/@idProject', array('ProjectController', 'add_comment'));

Flight::route('POST /project/contributor/search', array('ProjectController', 'search_contributor'));

Flight::route('GET /project/contributor/add/@idContribuor/@idProject', array('ProjectController', 'add_contributor'));

Flight::route('GET /project/contributor/remove/@idContribuor/@idProject', array('ProjectController', 'remove_contributor'));
/************************************ FILE **************************************************/

Flight::route('POST /file/add/@idProject', array('FileController', 'add'));

Flight::route('POST /file/edit/name/@idFile/@idProject', array('FileController', 'edit_name'));

Flight::route('GET /file/show/@idFile', array('FileController', 'show'));

Flight::route('GET /file/delete/@idFile', array('FileController', 'delete'));

Flight::route('POST /file/update/@idFile', array('FileController', 'update'));

/************************************ COMMENT **************************************************/

Flight::route('GET /comment/delete/@idComment', array('CommentController', 'delete'));

/************************************* CONTRIBUTOR ***********************************************/

Flight::route('GET /contributor/profile/update/@idContributor', array('ContributorController', 'profile_update_form'));

Flight::route('POST /contributor/profile/update/@idContributor', array('ContributorController', 'profile_update'));

