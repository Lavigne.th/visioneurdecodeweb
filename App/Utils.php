<?php
/**
 * Created by PhpStorm.
 * User: Theo
 * Date: 25/05/2019
 * Time: 16:50
 */

namespace App;
defined('ACCESSIBLE') or die('No direct script access.');

class Utils {

    /*
     * Renvoie l'extension d'un fichier
     */
    public  static function getExtenstion($name) {
        $strs = explode('.', $name);
        return $strs[sizeof($strs) - 1];
    }

    /*
     *  Renvoie les format supportés avec leurs ID en clée
     */
    public  static function getFormatWithIdAsKey() {
        $formats = \Model::factory('SupportedFormat')->findMany();
        $arr = [];
        foreach ($formats as $format) {
            $arr[$format->id] = $format;
        }
        return $arr ;
    }

    /*
     * renvoie le nom d'un fichier dans extension.
     */
    public  static function getNameWithoutExtension($fullname) {
        $strs = explode('.', $fullname);
        $strs = explode('/', $strs[sizeof($strs) - 2]);
        return $strs[sizeof($strs) - 1];
    }

    /*
     * Renvoie une date et heure sous forme de chaîne de caractère.
     */
    public static function getTimeDiffStr($time1, $time2 = null) {
        if (!$time2)
            $time2 = new \DateTime("now", new \DateTimeZone('Europe/Paris'));
        $str = 'Il y a ';
        $time = (array)$time1->diff($time2);

        if($time['y'] > 0) {
            if($time['y'] == 1) {
                $str.='1an ';
            } else {
                $str.=$time['y'].'ans ';
            }
        }
        if($time['m'] > 0) {
            $str.=$time['m'].'mois ';
        }
        if($time['d'] > 0) {
            $str.=$time['d'].'jours ';
        }
        if($time['h'] > 0) {
            if($time['h'] == 1) {
                $str.='1heure ';
            } else {
                $str.=$time['h'].'heures ';
            }
        }
        if($time['i'] > 0) {
            if($time['i'] == 1) {
                $str.='1minute ';
            } else {
                $str.=$time['i'].'minutes ';
            }
        }
        if($time['y'] == 0 && $time['m'] == 0 && $time['d'] == 0 && $time['h'] == 0 && $time['i'] == 0) {
            if($time['s'] == 1) {
                $str.='1seconde ';
            } else {
                $str.=$time['s'].'secondes ';
            }
        }
        return $str;
    }

    /*
     * Renvoie la date d'aujourd'hui en chaine de caractère
     */
    public static function nowStr() {
        $now = (array)new \DateTime("now", new \DateTimeZone('Europe/Paris'));
        return $now['date'];
    }

    /*
     * Charge la liste des thèmes.
     */
    public static function loadThemesPossibilitiest() {
        $files = scandir(ROOT.'/Public/assets/rainbow/css/');
        for ($i = 0; $i < sizeof($files); $i++) {
            if($files[$i][0] == '.') {
                unset($files[$i]);
            } else {
                $files[$i] = explode('.', $files[$i])[0];
            }
        }

        return $files;
    }

    /*
     * Renvoie le role sous forme de chaine de caractère à partir de l'abréviation stocké en DB.
     */
    public static function getRole($role) {
        if($role == \ProjectContributor::$ADMIN_ROLE)
            return 'administrateur';
        if($role == \ProjectContributor::$DEV_ROLE)
            return 'developpeur';
        if($role == \ProjectContributor::$VISITOR_ROLE)
            return 'visiteur';
    }

}