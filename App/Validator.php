<?php
/**
 * Created by PhpStorm.
 * Contributor: Theo
 * Date: 20/05/2019
 * Time: 14:55
 */

namespace App;
defined('ACCESSIBLE') or die('No direct script access.');

class Validator {

    private $errors, $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function getFields($field) {
        if(!isset($this->data[$field])){
            return null;
        } else {
            return $this->data[$field];
        }
    }

    /*
     * Vérifie que l'emain a une syntaxe valide
     */
    public function isEmail($field, $errorMsg = "Format d'email incorrect. ") {
        if(!filter_var($this->getFields($field), FILTER_VALIDATE_EMAIL)) {
            $this->errors[$field][] = $errorMsg;
        }
    }

    /*
     * Vérfie que la chaine ne con tient que des lettres
     */
    public function constainsLetterOnly($field, $errorMsg = "Ce champ ne doit comporter que des lettres. ") {
        if(!preg_match("/^[a-zA-Z]+$/", $this->getFields($field))) {
            $this->errors[$field][] = $errorMsg;
        }
    }

    /*
     * Vérifie que la chaine contient mon de X caractères
     */
    public function isShortEnough($field, $length, $errorMsg = "Ce champ est trop long. ") {
        if(strlen($this->getFields($field)) > $length) {
            $this->errors[$field][] = $errorMsg;
        }
    }

    /*
     * Vérifie que la chaine contient plus de X caractères
     */
    public function isLongEnough($field, $length, $errorMsg = "Ce champ n'est pas assez long. ") {
        if(strlen($this->getFields($field)) < $length){
            $this->errors[$field][] = $errorMsg;
        }
    }

    /*
     * Vérifie que les deux champs sont égaux.
     */
    public function areEqual($field1, $field2, $errorMsg = "Les champs doivent être identique. ") {
        if($this->getFields($field1) != $this->getFields($field2)){
            $this->errors[$field1][] = $errorMsg;
        }
    }

    /*
     * Vérifie que le champ passé n'existe pas en DB.
     */
    public function isUnique($field, $table, $errorMsg = "Valeur déja prise. ") {
        $exist = \Model::factory($table)->where($field, $this->getFields($field))->findMany();
        if($exist){
            $this->errors[$field][] = $errorMsg;
        }
    }

    /*
     * Vérifie que le champ n'est pas vide
     */
    public function isNotEmpty($field, $errorMsg = "Champ non renseigné. ") {
        if(empty($this->getFields($field))){
            $this->errors[$field][] = $errorMsg;
        }
    }

    /*
     * Si le format du fichier est supporté par la librairie rainbow
     */
    public function isSupportedFormat($field, $errorMsg = "Champ non renseigné. ") {
        $format = \Model::factory('SupportedFormat')->where('extension', Utils::getExtenstion($this->getFields($field)))->findOne();
        if(!$format){
            $this->errors[$field][] = $errorMsg;
        }
    }

    /*
     * Renvoie les erreurs trouvé dans les données envoyé au formulaire
     */
    public function getErrors() {
        return $this->errors;
    }

    /*
     * Renvoie si le formulaire
     */
    public function isValid() {
        return empty($this->errors);
    }

    /*
     * Enleve une erreur du formulaire.
     */
    public function removeError($field) {
        unset($this->errors[$field]);
    }

}