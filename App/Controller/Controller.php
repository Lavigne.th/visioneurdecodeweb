<?php
/**
 * Created by PhpStorm.
 * Contributor: Theo
 * Date: 03/05/2019
 * Time: 00:25
 */
namespace App\Controller;
defined('ACCESSIBLE') or die('No direct script access.');
use Josantonius\Session\Session;

use Flight;

class Controller {

    public  function  __construct()
    {
    }

    /*
     * Vérifie que l'utilisareur est connecté.
     */
    public static function isAuthenticated() {
        $contributor = Session::get('contributor');
        if(!$contributor) {
            self::setMessageFlash('alert-danger', 'Veuillez vous identifier');
            Flight::redirect('/');
            exit();
        }
    }

    /*
     * Renvoie l'id stocké en DB de l'utilisateur
     */
    public  static function  getUserId() {
        if(Session::get('contributor'))
            return Session::get('contributor')['id'];
        return null;
    }

    /*
     * Ajoute un message qui sera affiché lors du prochain chargement d'une page
     */
    public static function setMessageFlash($type, $message) {
        Session::set('flash', ['type' => $type, 'message' => $message]);
    }

    /*
     * Récupère tout les message à afficher. Ajoute aux données passé à twig les
     * information sur l'utilisateur.
     *
     */
    public static function render($path, $data = null) {

        $flash_message = Session::pull('flash');
        $data['flash'] = $flash_message;

        $data['contributor'] = Session::get('contributor');

        if($data)
            Flight::view()->display($path, $data);
        else
            Flight::view()->display($path);
    }

    /*
     * Redirige l'utilisateur vers une page du site, avec la possibilité
     * d'afficher un message à l'écran.
     */
    public static function redirect($url, $message = null, $type = null) {
        if($message)
            self::setMessageFlash('alert-danger', $message);
        if($message && $type)
            self::setMessageFlash($type, $message);
        Flight::redirect($url);
        exit();
    }
}