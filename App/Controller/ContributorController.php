<?php
/**
 * Created by PhpStorm.
 * User: Theo
 * Date: 27/05/2019
 * Time: 09:51
 */
defined('ACCESSIBLE') or die('No direct script access.');

use App\Controller\Controller;
use App\Validator;

class ContributorController extends Controller {

    public function __construct()
    {
        parent::__construct();
    }

    /*
     * Affiche le formulaire pour éditer un profil de contributeur.
     * Seul le contributeur lui même peut modifier son profil.
     */
    public static function profile_update_form($idContributor) {
        self::isAuthenticated();
        if($idContributor != self::getUserId())
            self::redirect('/', "Ce n'est pas votre compte.");

        $contributor = Model::factory('Contributor')
            ->findOne($idContributor)
            ->as_array();

        self::render('contributor/form.twig', ['data' => $contributor]);
    }

    /*
     * Modifie le profil d'un contributeur.
     * Seul le contributeur lui meme peut modifier son profil
     *
     */
    public static function profile_update($idContributor) {
        self::isAuthenticated();
        if(!$idContributor == self::getUserId())
            self::redirect('/', "Ce n'est pas votre compte.");

        $data = Flight::request()->data;
        $validator = self::validateContributor($data);

        if($validator->isValid()) {
            $contributor = Model::factory('Contributor')->findOne($idContributor);

            $contributor->name = $data['name'];
            $contributor->surname = $data['surname'];
            $contributor->email = $data['email'];
            $contributor->city = $data['city'];
            $contributor->address = $data['address'];
            $contributor->pseudo = $data['pseudo'];
            $contributor->save();

            self::redirect('/', "Les modifications ont été prises en compte.", "alert-success");
        }

        self::render('contributor/form.twig', ['data' => $data, 'errors' => $validator->getErrors()]);
    }

    /*
     * Valide des données d'un contributeur
     */
    private static function validateContributor($data) {
        $validator = new Validator($data);

        $validator->isLongEnough("pseudo", 3, "3 caractères minimun. ");
        $validator->isShortEnough("pseudo", 30, "30 caractères maximum. ");
        $validator->isLongEnough('name', 1);
        $validator->isShortEnough('name', 100);
        $validator->isLongEnough('surname', 1);
        $validator->isShortEnough('surname', 100);

        return $validator;
    }

}