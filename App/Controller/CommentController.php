<?php
/**
 * Created by PhpStorm.
 * User: Theo
 * Date: 26/05/2019
 * Time: 04:17
 */

use App\Controller\Controller;

defined('ACCESSIBLE') or die('No direct script access.');

class CommentController extends Controller {

    public function __construct()
    {
        parent::__construct();
    }

    /*
     * Fonction supprimant un commentaire d'un projet ou d'une update.
     * Seul les administrateur d'un projet peuvent éffécteur la suppréssion.
     */
    public static function delete($idComment) {
        self::isAuthenticated();
        $project = CommentAssos::getAssociatedItem(Project::class, $idComment);
        ProjectController::projectExistOrRedirect($project, "Le commentaire n'existe pas.");

        if(Project::isAdminOfProject($project->id, self::getUserId())) {
            $comment = Model::factory('Comment')->findOne($idComment);
            $comment->delete();
        } else {
            self::setMessageFlash('alert-danger', "Vous n'êtes pas l'admin du projet.");
        }

        Flight::redirect('/project/manage/'.$project->id);
    }

}