<?php
/**
 * Created by PhpStorm.
 * Contributor: Theo
 * Date: 03/05/2019
 * Time: 00:20
 */

defined('ACCESSIBLE') or die('No direct script access.');

use Josantonius\Session\Session;
use App\Validator;
use App\Controller\Controller;

class IndexController extends  Controller {

    public function __construct()
    {
        parent::__construct();
    }

    /*
     * Affiche la page d'acceuil
     */
    public static function index() {
        self::render("index/index.twig");
    }

    /*
     * Connecte un utilisateur.
     */
    public static function login() {
        $data = Flight::request()->data->getData();
        $contributor = Model::factory('Contributor')->where('pseudo', $data['pseudo'])->findOne();

        if($contributor && password_verify($data['password'], $contributor->password)) {
            Session::set('contributor', ['id' => $contributor->id, 'pseudo' => $contributor->pseudo]);
        } else {
            self::setMessageFlash('alert-danger', 'Mot de passe ou identifiant incorect.');
        }
        Flight::redirect('/');
    }

    /*
     * Deconnect un utilisateur.
     */
    public static function disconnect() {
        Session::pull('contributor');
        Flight::redirect('/');
    }

    /*
     * Enregistre un nouvel utilisateur si les données sont valides.
     */
    public static function register() {
        $data = Flight::request()->data->getData();
        $validator = new Validator($data);

        $validator->isLongEnough("pseudo", 3, "3 caractères minimun. ");
        $validator->isShortEnough("pseudo", 30, "30 caractères maximum. ");
        $validator->isLongEnough("password", 8, "Le mot de passe doit faire 8 caractères minimum. ");
        $validator->isShortEnough("password", 15, "Le mot de passe doit faire 8 caractères maximum. ");
        $validator->areEqual("password", "password-confirm", "Les mots de passe doivent être identique. ");
        $validator->isLongEnough('name', 1);
        $validator->isShortEnough('name', 100);
        $validator->isLongEnough('surname', 1);
        $validator->isShortEnough('surname', 100);
        $validator->isUnique("pseudo", "Contributor");

        if($validator->isValid()) {
            self::save_contributor($data);
            self::setMessageFlash('alert-success','Inscription avec succès.');
            Flight::redirect('/');
        }
        else
            self::render('index/form.twig', [
                'errors' => $validator->getErrors(),
                "data" => $data
            ]);
    }


    /*
     * Sauvegarde un nouveau contributeur en DB.
     */
    private static function save_contributor($data) {
        $contributor = Model::factory('Contributor')->create();
        $contributor->pseudo = $data['pseudo'];
        $contributor->password = password_hash($data['password'], PASSWORD_BCRYPT);
        $contributor->email = $data['email'];
        $contributor->name = $data['name'];
        $contributor->tel = $data["tel"];
        $contributor->surname = $data['surname'];
        $contributor->address = $data["address"];
        $contributor->city = $data["city"];
        $contributor->avatar = '/assets/img/avatar/avatar'.rand (1, 7).'.png';
        $contributor->save();
    }

}