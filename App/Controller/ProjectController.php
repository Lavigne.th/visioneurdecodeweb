<?php
/**
 * Created by PhpStorm.
 * User: Theo
 * Date: 21/05/2019
 * Time: 18:31
 */

defined('ACCESSIBLE') or die('No direct script access.');

use App\Controller\Controller;
use Josantonius\Session\Session;
use App\Validator;
use App\Utils;

class ProjectController extends Controller {


    public function __construct()
    {
        parent::__construct();
    }

    /*
     * Affiche le formulaire de création de projet.
     * L'utilisateur doit être connécté.
     */
    public static function add_form() {
        self::isAuthenticated();

        $themes = Utils::loadThemesPossibilitiest();

        self::render('project/form.twig', ['mode' => 'add', 'themes' => $themes]);
    }

    /*
     * Sauvegarde un nouveau projet en DB. Les données envoyés doivent être valide.
     */
    public static function add() {
        self::isAuthenticated();
        $data = Flight::request()->data;

        $validator = self::verifyProjectData($data);
        if(!$validator->isValid()) {
            self::render('project/form.twig', [
                    'project' => $data,
                    'errors' => $validator->getErrors(),
                    'mode' => 'add']
            );
        } else {
            try {
                ORM::get_db()->beginTransaction();

                $project = Model::factory('Project')->create();
                $project->name = $data["name"];
                $project->visibility = $data["visibility"];
                $project->theme = $data["theme"];
                $project->save();

                $projectContributor = Model::factory('ProjectContributor')->create();
                $projectContributor->project_id = $project->id;
                $projectContributor->contributor_id = Session::get('contributor')["id"];
                $projectContributor->role = "admin";
                $projectContributor->save();

                ORM::get_db()->commit();
            } catch (Exception $e) {
                ORM::get_db()->rollBack();
                echo "Failed: " . $e->getMessage();
                exit();
            }
            self::setMessageFlash("alert-success", "Projet ajouté.");
            Flight::redirect('/project/list/'.Session::get('contributor')['id']);
        }

    }

    /*
     * Affiche la liste des projet ou l'utilisateur est admin ou developpeur.
     */
    public  static function proj_list($idContributor) {

        $projectsContributors = Model::factory('ProjectContributor')
            ->where('contributor_id', $idContributor)
            ->findMany();

        $projects = [];
        foreach($projectsContributors as $projectsContributor) {
            $project = $projectsContributor->Project()->findOne()->as_array();
            $project["role"] = $projectsContributor->role;
            $projects[] = $project;
        }

        self::render('project/list.twig', ["projects" => $projects]);
    }

    /*
     * Supprime un projet. L'utilisateur doit être administrateur du projet.
     */
    public  static  function delete($idProject) {
        self::isAuthenticated();

        if(Project::isAdminOfProject($idProject, self::getUserId())) {
            Model::factory('Project')->findOne($idProject)->delete();
            self::setMessageFlash('alert-success', 'Projet supprimé.');
        } else {
            self::setMessageFlash('alert-success', 'La suppression à échouée.');
        }

        Flight::redirect('/project/list/'.self::getUserId());
    }


    /*
     * Formulaire pour modifier les caractéristiques d'un projet.
     * L'utilisateur doit être administrateur du projet et le projet doit exister en DB.
     */
    public  static  function edit_form($idProject) {
        self::isAuthenticated();

        $themes = Utils::loadThemesPossibilitiest();

        if(Project::isAdminOfProject($idProject, self::getUserId())) {
            $project = Model::factory('Project')->findOne($idProject);
            self::projectExistOrRedirect($project);
            self::render('project/form.twig', ['project' => $project, 'mode' => 'edit/'.$idProject, 'themes' => $themes]);
        } else {
            self::setMessageFlash("alert-danger", "Vous n'êtes pas admin du projet.");
            Flight::redirect('/');
            exit();
        }
    }

    /*
     * Sauvegarde les modifications apportés à aux caractéristiques d'un projet.
     * L'utilisateur doit être admin et le projet doit exister en DB et les données doivent être valides.
     */
    public  static  function edit($idProject) {
        self::isAuthenticated();
        $data = Flight::request()->data;

        if(Project::isAdminOfProject($idProject, self::getUserId())) {
            $validator = self::verifyProjectData($data);

            if($validator->isValid()) {
                $project = Model::factory('Project')->findOne($idProject);
                self::projectExistOrRedirect($project);
                $project->name = $data['name'];
                $project->visibility = $data['visibility'];
                $project->theme = $data['theme'];
                $project->save();

                self::setMessageFlash('alert-success', 'Modification éfféctuée.');

                Flight::redirect('/project/list/'.self::getUserId());
            } else {
                self::render('project/form.twig', [
                    'project' => $data,
                    'errors' => $validator->getErrors(),
                    'mode' => 'edit/'.$idProject]
                );
            }
        } else {
            self::setMessageFlash("alert-danger", "Vous n'êtes pas admin du projet.");
            Flight::redirect('/');
        }
    }

    /*
     * Vérifie la validé des caractéristique d'un projet.
     */
    private static function  verifyProjectData($data) {
        $validator = new Validator($data);

        $validator->isLongEnough("name", 3, "Le nom du projet doit faire au moin 3 caractère. ");
        $validator->isNotEmpty("visibility", "Veuillez choisir un niveau de visibilité. ");
        $validator->isNotEmpty("theme", "Veuillez choisir un thème. ");

        return $validator;
    }

    /*
     * Affiche la page d'un projet.
     * Le projet doit exister en DB, l'utilisareur doit avoir les droits suffisant pour consulter le projet.
     * Charge le projet, les commentaires, les fichiers, les contributeurs et les contributeurs ayant laissé un
     * commentaire.
     */
    public static function manage($idProject, $errors = null) {
        $project = Model::factory('Project')->findOne($idProject);
        self::projectExistOrRedirect($project);

        if(!Project::userCanViewProject($project, self::getUserId())) {
            self::setMessageFlash('alert-danger', "Vous n'avez pas accès à ce projet.");
            $adminId = Model::factory('ProjectContributor')->where('project_id', $idProject)->where('role', 'admin')->findOne()->contributor_id;
            Flight::redirect("/project/list/$adminId");
            exit();
        }


        $files = Model::factory('File')->where('project_id', $idProject)->findMany();
        $formats = Utils::getFormatWithIdAsKey();
        $admin = Project::getAdminOfProject($idProject);
        $commentsIds = Model::factory('CommentAssos')->where('table_name', Project::$_table)->where('item_id', $idProject)->findMany();
        $comments = [];

        $now = new DateTime("now", new DateTimeZone('Europe/Paris'));
        foreach ($commentsIds as $commentsId) {
            $com = Model::factory('Comment')->findOne($commentsId->id)->asArray();
            $date = new DateTime($com['posted_time'], new DateTimeZone('Europe/Paris'));
            $com['timeDiff'] = Utils::getTimeDiffStr($date, $now);
            $comments[] = $com;
        }
        //Les derniers commentaires postées sont en dernier dans le tableau, hors on veut les afficher en premier
        $comments = array_reverse($comments);

        $commentContributors = Comment::getAssociatedContributors($comments);
        $contributors = Project::getAllContributor($idProject);

        self::render('project/manage.twig', [
            'project' => $project,
            'formats' => $formats,
            'files'=> $files,
            'errors' => $errors,
            'admin' => $admin,
            'comments' => $comments,
            'commentContributors' => $commentContributors,
            'contributors' => $contributors,
            'isAdmin' => Project::isAdminOfProject($idProject, self::getUserId())
            ]);
    }

    /*
     * Vérifie que le projet passé en parametre n'est pas null.
     */
    public  static function projectExistOrRedirect($project, $message = "Le projet n'existe pas.") {
        if($project== null) {
            self::setMessageFlash('alert-danger', $message);
            Flight::redirect('/');
            exit();
        }
    }

    /*
     * Sauvegarde un commentaire associé à un projet en DB.
     * L'utilisateur doit etre au moin développeur, et le projet doit exiter.
     */
    public  static function add_comment($idProject) {
        $data = Flight::request()->data;

        $project = Model::factory('Project')->findOne($idProject);
        ProjectController::projectExistOrRedirect($project);

        if(Project::userIsAtLeastDeveloper($project, self::getUserId())) {
            try {
                self::saveProjectComment($data, $idProject);
            } catch (Exception $e) {
                ORM::get_db()->rollBack();
                echo "Failed: " . $e->getMessage();
                exit();
            }

            Flight::redirect('/project/manage/'.$idProject);
        } else {
            self::setMessageFlash('alert-danger', "Vous n'avez pas le droit de commenter ce projet");
        }
    }

    /*
     * Sauvegarde un commentaire associé à un projet en DB.
     */
    private  static function saveProjectComment($data, $idProject) {
        $comment = Model::factory('Comment')->create();
        $now = (array)new DateTime("now", new DateTimeZone('Europe/Paris'));

        ORM::get_db()->beginTransaction();

        $comment->content = $data['content'];
        $comment->posted_time = $now['date'];
        $comment->title = $data['title'];
        $comment->contributor_id = self::getUserId();
        $comment->save();

        $commentAssos = Model::factory('CommentAssos')->create();
        $commentAssos->id = $comment->id;
        $commentAssos->table_name = Project::$_table;
        $commentAssos->item_id = $idProject;
        $commentAssos->save();

        ORM::get_db()->commit();
    }

    /*
     *  Recherche les contributeur à partir d'une chaine de caractères et renvoie les résulat en JSON.
     */
    public  static function search_contributor() {
        if(isset($_POST['pseudo'])) {
            $data = $_POST['pseudo'];
            $people = ORM::for_table('Contributor')->where_like('pseudo', "%$data%")->findArray();
            echo json_encode(["result" => $people]);
        }
    }

    /*
     * Ajoute un contributeur à un projet.
     * L'utilisateur doit etre au moin développeur, et le projet et le contributeur doivent exister.
     * Le contributeur doit également ne pas déja etre associé au projet.
     *
     */
    public static function add_contributor($idContributor, $idProject) {
        self::isAuthenticated();
        if(!Project::isAdminOfProject($idProject, self::getUserId()))
            self::redirect('/', "Vous n'etes pas admin.");

        $exist = Model::factory('ProjectContributor')
            ->where('project_id', $idProject)
            ->where('contributor_id', $idContributor)
            ->select('id')
            ->findOne();

        if($exist)
            self::redirect('/project/manage/'.$idProject, 'Ce contributeur participe déjà au projet');
            if(Model::factory('Contributor')->findOne($idContributor)) {
                $projcontri = Model::factory('ProjectContributor')->create();
                $projcontri->project_id = $idProject;
                $projcontri->contributor_id = $idContributor;
                $projcontri->role = ProjectContributor::$DEV_ROLE;
                $projcontri->save();
            }
        self::redirect('/project/manage/'.$idProject, 'Ajout avec succès.', 'alert-success');
    }

    /*
     * Enleve un contributeur d'un projet.
     * L'utilisateur doit etre administrateur et le l'association du contributeur au projet doit exister
     */
    public  static function remove_contributor($contriId,$projId) {
        self::isAuthenticated();
        if(!Project::isAdminOfProject($projId, self::getUserId()))
            self::redirect('/project/manage/'.$projId, "Vous n'êtes pas l'admin du projet.");

        $contri = Model::factory('ProjectContributor')
            ->where('project_id', $projId)
            ->where('contributor_id', $contriId)
            ->findOne();

        if($contri) {
            if($contri->role == ProjectContributor::$ADMIN_ROLE)
                self::redirect('/project/manage/'.$projId, "Impossible de supprimer l'administrateur du projet.");
            $contri->delete();
            self::redirect('/project/manage/'.$projId, "Contributeur retiré.", "alert-success");
        }
        self::redirect('/project/manage/'.$projId, "Contributeur introuvable.");
    }
}