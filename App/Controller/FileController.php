<?php
/**
 * Created by PhpStorm.
 * User: Theo
 * Date: 21/05/2019
 * Time: 18:30
 */

defined('ACCESSIBLE') or die('No direct script access.');

use App\Controller\Controller;
use App\Utils;

class FileController extends Controller {

    public function __construct()
    {
        parent::__construct();
    }

    /*
     * Ajoute un fichier à un projet. Vérifie au préalable la possibilité d'ajouter un fichier.
     * Vérifie également que le format est supporté par la librairie rainbow.
     */
    public static function add($idProject) {
        self::isAuthenticated();

        self::canUploadFile($idProject);

        $file = Flight::request()->files['file'];

        $validator = new \App\Validator($file);
        $validator->isSupportedFormat('name', "Ce format n'est pas supporté.");

        if($validator->isValid()) {
            $data = file_get_contents($file['tmp_name']);
            $format = Model::factory('SupportedFormat')->where('extension', Utils::getExtenstion($file['name']))->findOne();

            $newFile = Model::factory('File')->create();
            $newFile->name = $file['name'];
            $newFile->content = $data;
            $newFile->project_id = $idProject;
            $newFile->format_id = $format->id;
            $newFile->save();

            Flight::redirect("/project/manage/$idProject ");
            exit();
        } else {
            ProjectController::manage($idProject, $validator->getErrors());
        }
    }

    /*
     * Vérifie que le projet existe et que l'utilisateur est au moin développeur.
     */
    private static function canUploadFile($idProject) {
        $project = Model::factory('Project')->findOne($idProject);
        ProjectController::projectExistOrRedirect($project);


        if(!Project::userIsAtLeastDeveloper($project, self::getUserId())) {
            self::setMessageFlash('alert-danger', "Vous n'avez pas le droit du'pload sur ce projet.");
            Flight::redirect('/');
            exit();
        }
    }

    /*
     * Modifie le nom d'un fichier. L'utilisateur doit être au moin dévelopeur pour éffécteur cet action.
     */
    public static function edit_name($idFile, $idProject) {
        self::isAuthenticated();
        $project = Model::factory('Project')->findOne($idProject);
        if(Project::userIsAtLeastDeveloper($project, self::getUserId())) {
            $data = Flight::request()->data;
            $file = Model::factory('File')->findOne($idFile);
            $file->name = $data->name.'.'.Utils::getExtenstion($file->name);
            $file->save();
        } else {
            self::setMessageFlash('alert-danger', "Vous n'avez pas les droits sur ce fichier.");
        }

        Flight::redirect("/project/manage/$idProject");
    }

    /*
     * Permet d'afficher un fichier de code à l'écran.
     * Le fichier doit exister dans la base de donnée et l'utilisateur doit avoir les droits suffisants pour
     * accéder au fichier.
     * Charge également les commentaires associés au projet, ainsi que le contributeurs associés au commentaires.
     */
    public  static function show($idFile) {
        $file = Model::factory('File')->findOne($idFile);
        if($file)
            $project = Model::factory('Project')->findOne($file->project_id);

        if($file && Project::userCanViewProject($project, self::getUserId())) {
            $format = Model::factory('SupportedFormat')->findOne($file->format_id);
            $adminId = Model::factory('ProjectContributor')->where('project_id', $project->id)->where('role', 'admin')->findOne()->contributor_id;
            $admin = Model::factory('Contributor')->findOne($adminId);
            $updates = Model::factory('FileUpdate')->where('file_id', $idFile)->findMany();

            $comments = self::getUpdateComments($updates);
            $updates = array_reverse($updates);
            $contributors = Comment::getAssociatedContributors($comments);

            self::render('file/display.twig', [
                'file' => $file,
                'project' => $project,
                'format'=> $format,
                'admin' => $admin,
                'comments' => $comments,
                'updates' => $updates,
                'contributors' => $contributors
            ]);
        } else {
            self::setMessageFlash('alert-danger', "Impossible d'acceder à ce fichier.");
            Flight::redirect('/');
        }
    }

    /*
     * Charge les commentaires d'un fichier.
     */
    private static function getUpdateComments($updates) {
        $comments = [];
        foreach ($updates as $update) {
            $comment = Model::factory('Comment')->findOne($update->comment_id)->asArray();
            $date = new DateTime($comment['posted_time'], new DateTimeZone('Europe/Paris'));
            $comment['timeDiff'] = Utils::getTimeDiffStr($date);
            $comments[$update->id] = $comment;
        }

        return $comments;
    }

    /*
     * Supprime un fichier d'un projet. Le fichier doit exister en DB et l'utilisateur doit
     * au moins etre developpeur du projet.
     */
    public static function delete($idFile) {
        self::isAuthenticated();

        $file = Model::factory('File')->findOne($idFile);
        self::fileExistOrRedirect($file);
        $project = Model::factory('Project')->findOne($file->project_id);

        if(Project::userIsAtLeastDeveloper($project, self::getUserId())) {
            $file->delete();
        } else {
            self::setMessageFlash('alert-danger', "Vous n'avez pas les droits nécéssaire pour éfféctuer cette action.");
        }
        Flight::redirect('/project/manage/'.$project->id);
    }

    /*
     * Met a jours un fichier d'un projet. Le fichier doit exiter en DB et l'utilisateur doit
     * être au moins développeur.
     * Valide ensuite les données que le format est supporté par la librairie rainbow et que le commentaire
     * est valide.
     */
    public static function update($fileId) {
        self::isAuthenticated();
        $fileDB = Model::factory('File')->findOne($fileId);
        self::fileExistOrRedirect($fileDB);
        $project = Model::factory('Project')->findOne($fileDB->project_id);

        if(!Project::userIsAtLeastDeveloper($project, self::getUserId()))
            self::redirect('/project/manage/'.$project->id,"Vous n'avez pas les droits nécéssaire.");

        $data = Flight::request()->data;
        $file = Flight::request()->files['file'];

        $validatorFile = new \App\Validator($file);
        $validatorFile->isSupportedFormat('name', "Ce format n'est pas supporté.");

        $validatorData = new \App\Validator($data);
        $validatorData->isLongEnough('title', 3);
        $validatorData->isLongEnough('comment-content', 3);

        if($validatorData->isValid() && $validatorFile->isValid()) {
            try {
                self::saveUpdate($fileDB, $data, $file, $fileId);
            } catch (Exception $e) {
                ORM::get_db()->rollBack();
                echo "Failed: " . $e->getMessage();
                exit();
            }

            self::setMessageFlash('alert-success', 'Mise à jours éfféctuée');

        } else {
            self::setMessageFlash('alert-danger', 'Les données sont invalide.');
        }
        Flight::redirect("/project/manage/".$project->id);
    }

    /*
     * Créé une nouvelle update d'un fichier en DB.
     */
    private static function saveUpdate($fileDB, $data, $file, $fileId) {
        $update = Model::factory('FileUpdate')->create();
        $comment = Model::factory('Comment')->create();
        $commentAssos = Model::factory('CommentAssos')->create();

        ORM::get_db()->beginTransaction();

        $fileDB->content = file_get_contents($file['tmp_name']);
        $fileDB->save();

        $comment->title = $data["title"];
        $comment->content = $data['comment-content'];
        $comment->posted_time = Utils::nowStr();
        $comment->contributor_id = self::getUserId();
        $comment->save();

        $update->file_id = $fileId;
        $update->comment_id = $comment->id;
        $update->save();

        $commentAssos->id = $comment->id;
        $commentAssos->table_name = FileUpdate::$_table;
        $commentAssos->item_id = $update->id;
        $commentAssos->save();

        ORM::get_db()->commit();
    }

    /*
     * Vérifie que le fichier passer en parametre n'est pas nul.
     */
    public  static function fileExistOrRedirect($file) {
        if(!$file) {
            self::setMessageFlash('alert-danger', "Le fichier n'existe pas.");
            Flight::redirect('/');
            exit();
        }
    }
}