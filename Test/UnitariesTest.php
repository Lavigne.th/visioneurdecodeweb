<?php
/**
 * Created by PhpStorm.
 * Contributor: Theo
 * Date: 20/05/2019
 * Time: 15:47
 */

require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use App\Validator;


class UnitariesTest extends TestCase {

    public function test_is_unique(){
        /*$user = Model::factory('Contributor')->findMany();

        $this->assertEquals($user[0]->pseudo, 'admin');*/
    }

    public function test_validator_is_email(){
        $v = new Validator(["email" => "email@gmail.com", "email2" => "email.com", "email3" => "email@gmail."]);

        $v->isEmail("email");
        $this->assertEquals($v->isValid(), true);

        $v->isEmail("email2");
        $this->assertEquals($v->isValid(), false);
        $this->assertEquals($v->getErrors()["email2"][0], "Format d'email incorrect. ");

        $v->removeError("email2");
        $this->assertEquals($v->isValid(), true);

        $v->isEmail("email3", "erreur");
        $v->isLongEnough("email3", 100);
        $this->assertEquals($v->isValid(), false);
        $this->assertEquals($v->getErrors()["email3"][0], "erreur");
        $this->assertEquals($v->getErrors()["email3"][1], "Ce champ n'est pas assez long. ");
    }

    public function test_is_long_enough(){
        $v = new Validator(["email" => "email@gmail.com", "email2" => "email2", "email3" => "email@gmail."]);

        $v->isLongEnough("email", 3);
        $this->assertEquals($v->isValid(), true);

        $v->isLongEnough("email2", 6);
        $this->assertEquals($v->isValid(), true);

        $v->isLongEnough("email3", 100, "erreur");
        $this->assertEquals($v->isValid(), false);
        $this->assertEquals($v->getErrors()["email3"][0], "erreur");

        $v->removeError("email3");
        $this->assertEquals($v->isValid(), true);

        $v->isLongEnough("email3", 100);
        $v->isEmail("email3", "erreur");
        $this->assertEquals($v->isValid(), false);
        $this->assertEquals($v->getErrors()["email3"][0], "Ce champ n'est pas assez long. ");
        $this->assertEquals($v->getErrors()["email3"][1], "erreur");
    }

    public function test_is_short_enough(){
        $v = new Validator(["email" => "email@gmail.com", "email2" => "email2", "email3" => "email@gmail."]);

        $v->isShortEnough("email", 100);
        $this->assertEquals($v->isValid(), true);

        $v->isShortEnough("email2", 6);
        $this->assertEquals($v->isValid(), true);

        $v->isShortEnough("email3", 10, "erreur");
        $this->assertEquals($v->isValid(), false);
        $this->assertEquals($v->getErrors()["email3"][0], "erreur");

        $v->removeError("email3");
        $this->assertEquals($v->isValid(), true);

        $v->isShortEnough("email3", 10);
        $v->isEmail("email3", "erreur");
        $this->assertEquals($v->isValid(), false);
        $this->assertEquals($v->getErrors()["email3"][0], "Ce champ est trop long. ");
        $this->assertEquals($v->getErrors()["email3"][1], "erreur");
    }

    public function test_constains_letter_only(){
        $v = new Validator([
            "data1" => "ozfjefoijezf",
            "data2" => "",
            "data3" => "dz ",
            "data4" => "zf876",
            "data5" => "kjezf$"
        ]);

        $v->constainsLetterOnly("data1");
        $this->assertEquals($v->isValid(), true);

        $v->constainsLetterOnly("data2");
        $this->assertEquals($v->isValid(), false);
        $this->assertEquals($v->getErrors()["data2"][0], "Ce champ ne doit comporter que des lettres. ");

        $v->removeError("data2");
        $this->assertEquals($v->isValid(), true);

        $v->constainsLetterOnly("data3", "erreur");
        $this->assertEquals($v->isValid(), false);
        $this->assertEquals($v->getErrors()["data3"][0], "erreur");

        $v->removeError("data3");
        $this->assertEquals($v->isValid(), true);

        $v->constainsLetterOnly("data4", "erreur");
        $this->assertEquals($v->isValid(), false);
        $this->assertEquals($v->getErrors()["data4"][0], "erreur");

        $v->removeError("data4");
        $this->assertEquals($v->isValid(), true);

        $v->constainsLetterOnly("data5", "erreur");
        $this->assertEquals($v->isValid(), false);
        $this->assertEquals($v->getErrors()["data5"][0], "erreur");
    }

    public function test_are_equal(){
        $v = new Validator([
            "data1" => "ozfjefoijezf",
            "data2" => "ozfjefoijezf",
            "data3" => "pozekf",
            "data4" => ""
        ]);

        $v->areEqual("data1", "data2");
        $this->assertEquals($v->isValid(), true);

        $v->areEqual("data1", "data3");
        $this->assertEquals($v->isValid(), false);
        $this->assertEquals($v->getErrors()["data1"][0], "Les champs doivent être identique. ");

        $v->removeError("data1");
        $this->assertEquals($v->isValid(), true);


        $v->areEqual("data1", "data4", "erreur");
        $this->assertEquals($v->isValid(), false);
        $this->assertEquals($v->getErrors()["data1"][0], "erreur");
    }

    public function test_is_not_empty(){
        $v = new Validator([
            "data1" => "",
            "data2" => "
            ",
            "data3" => "pozekf",
            "data4" => " "
        ]);

        $v->isNotEmpty("data1", "Erreur");
        $this->assertEquals($v->isValid(), false);
        $this->assertEquals($v->getErrors()["data1"][0], "Erreur");

        $v->removeError("data1");
        $this->assertEquals($v->isValid(), true);

        $v->isNotEmpty("data2", "Erreur");
        $this->assertEquals($v->isValid(), true);

        $v->isNotEmpty("data3", "Erreur");
        $this->assertEquals($v->isValid(), true);

        $v->isNotEmpty("data4", "Erreur");
        $this->assertEquals($v->isValid(), true);
    }


    public function test_get_extencion() {

        $this->assertEquals(\App\Utils::getExtenstion('file.java'), "java");
        $this->assertEquals(\App\Utils::getExtenstion('path/file.pas'), "pas");
        $this->assertEquals(\App\Utils::getExtenstion('path/folder/file.py'), "py");
        $this->assertEquals(\App\Utils::getExtenstion('file.js'), "js");
    }

    public function test_get_name_without_extension() {

        $this->assertEquals(\App\Utils::getNameWithoutExtension('file.java'), "file");
        $this->assertEquals(\App\Utils::getNameWithoutExtension('path/file.pas'), "file");
        $this->assertEquals(\App\Utils::getNameWithoutExtension('path/folder/file.py'), "file");
        $this->assertEquals(\App\Utils::getNameWithoutExtension('file.js'), "file");
    }

    public function test_get_time_diff_str() {

        $date1 = new DateTime("2019-05-20 22:00:00");
        $date2 = new DateTime("2019-05-25 23:10:00");

        $this->assertEquals(\App\Utils::getTimeDiffStr($date1, $date2), "Il y a 5jours 1heure 10minutes ");
    }
}