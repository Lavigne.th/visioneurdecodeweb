DROP TABLE IF EXISTS "Contributor";
DROP TABLE IF EXISTS "Project";
DROP TABLE IF EXISTS "File";
DROP TABLE IF EXISTS "Comment";
DROP TABLE IF EXISTS "Comment_assos";
DROP TABLE IF EXISTS "Project_contributor";
DROP TABLE IF EXISTS "File_update";
DROP TABLE IF EXISTS "Supported_format";

PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS "Contributor" (
  'id' INTEGER  NOT NULL PRIMARY  KEY AUTOINCREMENT UNIQUE,
  'pseudo' VARCHAR(30) NOT NULL UNIQUE,
  'name' VARCHAR(100) NOT NULL,
  'surname' VARCHAR(30) NOT NULL,
  'tel'  VARCHAR(20),
  'address'  VARCHAR(50),
  'city' VARCHAR(30),
  'password' VARCHAR(200) NOT NULL,
  'email' VARCHAR(200) NOT NULL,
  'avatar' VARCHAR(30)
);

CREATE  TABLE IF NOT EXISTS "Project" (
  'id' INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
  'name' VARCHAR(30) NOT NULL,
  'visibility' VARCHAR(30) NOT NULL,
  'theme' VARCHAR(30) NOT NULL
);

CREATE  TABLE IF NOT EXISTS "File" (
  'id' INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
  'name' VARCHAR(30) NOT NULL,
  'content' TEXT NOT NULL,
  'project_id' INTEGER NOT NULL,
  'format_id' INTEGER NOT NULL,

  CONSTRAINT constraint_project
  FOREIGN KEY ('project_id')
  REFERENCES 'Project'('id')
    ON DELETE CASCADE,

  CONSTRAINT constraint_format
  FOREIGN KEY ('format_id')
  REFERENCES 'Supported_format'('id')
  ON DELETE CASCADE
);

CREATE  TABLE IF NOT EXISTS "File_update" (
  'id' INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
  'file_id' INTEGER NOT NULL,
  'comment_id' INTEGER,

  CONSTRAINT constraint_file
  FOREIGN KEY ('file_id')
  REFERENCES 'File'('id')
    ON DELETE CASCADE,

  CONSTRAINT constraint_comment
  FOREIGN KEY ('comment_id')
  REFERENCES 'Comment'('id')
    ON DELETE CASCADE
);



CREATE  TABLE IF NOT EXISTS "Project_contributor" (
  'id' INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
  'project_id' INTEGER  NOT NULL,
  'contributor_id' INTEGER  NOT NULL,
  'role' VARCHAR(30) NOT NULL,

  CONSTRAINT constraint_project
  FOREIGN KEY ('project_id')
  REFERENCES 'Project'('id')
    ON DELETE CASCADE,

  CONSTRAINT constraint_contributor
  FOREIGN KEY ('contributor_id')
  REFERENCES 'Contributor'('id')
    ON DELETE CASCADE
);

CREATE  TABLE IF NOT EXISTS "Comment" (
  'id' INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
  'title' VARCHAR(30) NOT NULL,
  'content' TEXT NOT NULL,
  'posted_time' DATETIME NOT NULL,
  'contributor_id' INTEGER NOT NULL ,

  CONSTRAINT constraint_contributor
  FOREIGN KEY ('contributor_id')
  REFERENCES 'Contributor'('id')
    ON DELETE NO ACTION
);

CREATE  TABLE IF NOT EXISTS "Comment_assos" (
  'id' INTEGER UNIQUE,
  'table_name' VARCHAR(30) NOT NULL,
  'item_id' INTEGER NOT NULL,

  CONSTRAINT constraint_comment
  FOREIGN KEY ('id')
  REFERENCES 'Comment'('id')
    ON DELETE CASCADE
);


CREATE  TABLE IF NOT EXISTS "Supported_format" (
  'id' INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
  'name' INTEGER  NOT NULL,
  'extension' INTEGER  NOT NULL,
  'icon' INTEGER  NOT NULL
);



