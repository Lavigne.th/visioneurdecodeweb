<?php
/**
 * Created by PhpStorm.
 * User: Theo
 * Date: 27/05/2019
 * Time: 15:42
 */

require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest {

    public function test_index()
    {
        $response = $this->make_request("GET", "/");
        $data = $response->getBody()->getContents();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("Lorem ipsum dolor sit amet, consectetur adipiscing elit.", $data);
        $this->assertContains("Creer un compte", $data);
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }

    public function test_list()
    {
        $response = $this->make_request("GET", "/project/list/1");
        $this->assertEquals(200, $response->getStatusCode());
        $data = $response->getBody()->getContents();
        $this->assertcontains("Mes projets", $data);
        $this->assertcontains("ID", $data);
        $this->assertcontains("Nom", $data);
        $this->assertcontains("Visibilité", $data);
        $this->assertcontains("text/html", $response->getHeader('Content-Type')[0]);
    }

    public function test_fileManager()
    {
        $response = $this->make_request("GET", "/project/manage/1");
        $this->assertEquals(200, $response->getStatusCode());
        $data = $response->getBody()->getContents();
        $this->assertcontains("Le commentaire doit faire au moin 3 caractères.", $data);
        $this->assertcontains("Choisissez un fichier...", $data);
        $this->assertcontains("Sauvegarder", $data);
        $this->assertcontains("Nom du fichier", $data);
        $this->assertcontains("text/html", $response->getHeader('Content-Type')[0]);
    }

    public function test_project_edit()
    {
        $response = $this->make_request("GET", "/project/edit/1");
        $this->assertEquals(200, $response->getStatusCode());
        $data = $response->getBody()->getContents();
        $this->assertContains("Lorem ipsum dolor sit amet, consectetur adipiscing elit.", $data);
        $this->assertContains("Creer un compte", $data);
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }

    public function test_project_create()
    {
        $response = $this->make_request("GET", "/project/add");
        $this->assertEquals(200, $response->getStatusCode());
        $data = $response->getBody()->getContents();
        $this->assertContains("Lorem ipsum dolor sit amet, consectetur adipiscing elit.", $data);
        $this->assertContains("Creer un compte", $data);
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }

    public function test_display_file()
    {
        $response = $this->make_request("GET", "/file/show/5");
        $this->assertEquals(200, $response->getStatusCode());
        $data = $response->getBody()->getContents();
        $this->assertcontains("Code", $data);
        $this->assertcontains("Mises à jours", $data);
        $this->assertcontains("De :", $data);
        $this->assertcontains("text/html", $response->getHeader('Content-Type')[0]);
    }
    /*public function test_login_post()
    {
        $data = array('pseudo' => 'UnitaryTest', 'password' => 'aaaaaaaa');
        $response = Requests::post('/login', array(), $data);

        $data = $response->getBody()->getContents();
        $this->assertContains('<a class="btn btn-danger" href="/disconnect" role="button">deconexion</a>', $data);
        $this->assertContains('<a class="nav-link" href="/project/add">Nouveau projet</a>', $data);
    }*/
}